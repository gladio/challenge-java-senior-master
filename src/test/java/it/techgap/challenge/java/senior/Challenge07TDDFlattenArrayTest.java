package it.techgap.challenge.java.senior;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by ipassari on 15/01/2018.
 */
public class Challenge07TDDFlattenArrayTest {

//    Write some code that will flatten an array of arbitrarily nested arrays of integers into a
//    flat array of integers. e.g.
//
//    [[1,2,[3]],4] -> [1,2,3,4]
//
//    Your program should be fully tested too.

    @Test
    public void flattenArrayTest() {
        Object[] objects = {1, 2, new Object[]{3, 4, new Object[]{5}, 6, 7}, 8, 9, 10};
        Integer[] intArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertArrayEquals(intArray, Challenge07TDDFlattenArray.flattenArray(objects));
    }

}

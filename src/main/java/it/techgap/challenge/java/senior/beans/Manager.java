package it.techgap.challenge.java.senior.beans;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by gladio on 13/01/18.
 */
public class Manager extends Salesman {
    protected Set<Employee> employers = new HashSet<>();

    public Manager(String name, int salary, Set<Employee> employers) {
        super(0, name, salary);
        this.employers = employers;
    }

    public Set<Employee> getEmployers() {
        return employers;
    }

    private Set<Employee> getTotalEmployers(Set<Employee> employees) {
        Set<Employee> totalEmployees = new HashSet<>(employees);
        for (Employee employer : employees) {
            if (employer instanceof Manager) {
                Manager m = (Manager) employer;
                totalEmployees.addAll(getTotalEmployers(m.getEmployers()));
            }
        }
        return totalEmployees;
    }

    @Override
    public double getSalary() {
        double totalSalaries = getTotalEmployers(employers).stream().mapToDouble(Employee::getSalary).sum();
        return (totalSalaries * 0.5D) / 100 + this.salary;
    }


}

package it.techgap.challenge.java.senior.beans;

/**
 * Created by gladio on 13/01/18.
 */
public class Salesman extends Technician {

    protected int monthlySales;

    public Salesman(int monthlySales, String name, int salary) {
        this.monthlySales = monthlySales;
        this.name = name;
        this.salary = salary;
    }

    public Salesman() {
    }

    public int getMonthlySales() {
        return monthlySales;
    }

    @Override
    public double getSalary() {
        return (this.monthlySales * 10D) / 100 + this.salary;
    }


}

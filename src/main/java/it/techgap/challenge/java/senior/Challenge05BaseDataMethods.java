package it.techgap.challenge.java.senior;

import it.techgap.challenge.java.senior.beans.*;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Challenge05BaseDataMethods {

    public static void executeWait(OneSecondWait waitp, int times) {
        if (waitp == null )return;
        try {
            ExecutorService executorService = Executors.newFixedThreadPool(4);
            Callable<Boolean> task = () -> {
                try {
                    waitp.waitOneSecond();
                    return true;
                } catch (InterruptedException e) {
                    throw new IllegalStateException("interrupted task", e);
                }
            };
            IntStream.range(0, times).forEach(i -> executorService.submit(task));
            executorService.shutdown();
            executorService.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static <V> ValueCounter<V> newValueCounter() {
        return new ValueCounterImpl();
    }

    public static <K, V> TimeBasedKVCache<K, V> newTimeBasedKVCache() {
        return new TimeBasedKVCacheImpl();
    }

}

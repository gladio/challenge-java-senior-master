package it.techgap.challenge.java.senior;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Challenge00BaseMethods {

    public static int round(double d) {
        return (int) Math.round(d);
    }

    public static int round(String d) {
        if (d == null) return 0;
        return (int) Math.round(Double.valueOf(d));
    }

    //Conta gli shift a destra fino a quando il non arriva a 0
    public static int bitNeeded(int i) {
        int count = 0;
        while (i > 0) {
            count++;
            i = i >> 1;
        }
        return count;
    }

    public static boolean palindromic(int num) {
        int palindrome = num;
        int reverse = 0;

        while (palindrome != 0) {
            int tmp = palindrome % 10;
            reverse = reverse * 10 + tmp;
            palindrome = palindrome / 10;
        }

        if (num == reverse) {
            return true;
        }
        return false;
    }

    public static int hex(String i) {
        if (i == null) return 0;
        return Integer.parseInt(i, 16);
    }

    public static String swapXY(String i) {
        if (i == null) return null;
        for (int a = 0; a < i.length(); a++) {
            char c = i.charAt(a);
            if (c == 'x' || c == 'X') {
                i = replaceCharAt(i, a, 'y');
            }
            if (c == 'y' || c == 'Y') {
                i = replaceCharAt(i, a, 'x');
            }
        }
        return i;
    }

    public static Number[] findNumbers(String i) {
        if (i == null) return null;
        List<Number> numbersList = new ArrayList<>();
        Pattern p = Pattern.compile("[+]?[0-9]*\\.?[0-9]+([eE][+]?[0-9]+)?");
        Matcher m = p.matcher(i);
        while (m.find()) {
            if (m.group().contains(".")) {
                numbersList.add(Double.valueOf(m.group()));
            } else {
                numbersList.add(Integer.valueOf(m.group()));
            }
        }
        Number[] numbers = new Number[numbersList.size()];
        return numbersList.toArray(numbers);
    }

    private static String replaceCharAt(String s, int pos, char c) {
        if (s == null) return null;
        return s.substring(0, pos) + c + s.substring(pos + 1);
    }

}

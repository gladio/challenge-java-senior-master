package it.techgap.challenge.java.senior.beans;

/**
 * Created by gladio on 13/01/18.
 */
public class Technician implements Employee {

    protected String name;
    protected double salary;

    public Technician() {
    }

    public Technician(String name, int salary) {
        this.name = name;
        this.salary = (double) salary;
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    public String getName() {
        return this.name;
    }
}

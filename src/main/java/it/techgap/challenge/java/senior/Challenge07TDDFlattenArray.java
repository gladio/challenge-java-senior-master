package it.techgap.challenge.java.senior;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ipassari on 15/01/2018.
 */
public class Challenge07TDDFlattenArray {

    public static Integer[] flattenArray(Object[] inputArray) throws IllegalArgumentException {
        if (inputArray == null) return null;
        List<Integer> integerList = new ArrayList<>();
        for (Object element : inputArray) {
            if (element instanceof Integer) {
                integerList.add((Integer) element);
            } else if (element instanceof Object[]) {
                integerList.addAll(Arrays.asList(flattenArray((Object[]) element)));
            } else {
                throw new IllegalArgumentException();
            }
        }
        return integerList.toArray(new Integer[integerList.size()]);
    }
}

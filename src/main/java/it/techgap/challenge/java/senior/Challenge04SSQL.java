package it.techgap.challenge.java.senior;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This test uses an in-memory <a href="http://www.h2database.com/">H2 database</a>
 */
public class Challenge04SSQL {

    protected static Logger logger = Logger.getLogger(Challenge04SSQL.class.toString());

    /**
     * TODO: Implement this
     */
    public static class ManagerWithCount {

        String cf;
        long count;

        public ManagerWithCount(String cf, long count) {
            this.cf = cf;
            this.count = count;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ManagerWithCount that = (ManagerWithCount) o;

            if (count != that.count) return false;
            return cf != null ? cf.equals(that.cf) : that.cf == null;

        }

        @Override
        public int hashCode() {
            int result = cf != null ? cf.hashCode() : 0;
            result = 31 * result + (int) (count ^ (count >>> 32));
            return result;
        }
    }


    /**
     * Gets the CF of all employees aged more than minAge
     *
     * @param connection A JDBC Connection
     * @param minAge     Minimum age to consider
     * @return A set of employees's CF
     * @throws SQLException If anything goes wrong
     */
    public static Collection<String> selectCFOfEmployeesAgedMoreThan(Connection connection, int minAge) throws SQLException {
        if (connection == null){
            logger.log(Level.SEVERE, "Connection must not be null! ");
            return null;
        }
        List<String> ls = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        try {
            String query = "SELECT * FROM EMPLOYEE WHERE AGE > ?";
            logger.log(Level.INFO, "Query : "+ query);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, minAge);
            ResultSet resultSet = preparedStatement.executeQuery();
            logger.log(Level.INFO, "Numbers of records : "+ resultSet.getRow());
            while (resultSet.next()) {
                String cf = resultSet.getString("CF");
                ls.add(cf);
                logger.log(Level.INFO, "Found Employer with CF: "+ cf);
            }

        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return ls;
    }

    /**
     * Gets the CF of all employees whose manager has a salary of more than minSalary euros
     *
     * @param connection A JDBC Connection
     * @param minSalary  Minimum salary to consider
     * @return A set of employees' CF
     * @throws SQLException If anything goes wrong
     */
    public static Collection<String> selectCFOfEmployeesWhoseManagerHasASalaryofMoreThan(
            Connection connection,
            int minSalary) throws SQLException {
        if (connection == null){
            logger.log(Level.SEVERE, "Connection must not be null! ");
            return null;
        }
        List<String> ls = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        try {
            String query = "SELECT * FROM EMPLOYEE WHERE MANAGER_CF IN (SELECT CF FROM EMPLOYEE WHERE SALARY > ? AND MANAGER_CF is null)";
            logger.log(Level.INFO, "Query : "+ query);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, minSalary);
            ResultSet resultSet = preparedStatement.executeQuery();
            logger.log(Level.INFO, "Numbers of records : "+ resultSet.getRow());
            while (resultSet.next()) {
                String cf = resultSet.getString("CF");
                ls.add(cf);
                logger.log(Level.INFO, "Found Employer with CF: "+ cf);
            }

        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return ls;
    }

    /**
     * Retrieves all the managers with their respective subordinates count if and only if the manager has more than
     * minSubordinates subordinates
     *
     * @param connection      A JDBC Connection
     * @param minSubordinates Minimum number of subordinates to consider
     * @return A well-built collection of {@link ManagerWithCount}
     * @throws SQLException If anything goes wrong
     */
    public static Collection<ManagerWithCount> getManagersWithMoreThanXSubordinates(
            Connection connection,
            int minSubordinates
    ) throws SQLException {
        if (connection == null){
            logger.log(Level.SEVERE, "Connection must not be null! ");
            return null;
        }

        List<ManagerWithCount> ls = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        try {

            String query = "SELECT TBL.MANAGER_CF,TBL.counter FROM (SELECT MANAGER_CF, count(MANAGER_CF) as counter FROM EMPLOYEE GROUP BY MANAGER_CF) AS TBL WHERE TBL.counter >?";
            logger.log(Level.INFO, "Query : "+ query);
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, minSubordinates);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String cf = resultSet.getString("MANAGER_CF");
                Long counter = resultSet.getLong("counter");
                ls.add(new ManagerWithCount(cf, counter));
                logger.log(Level.INFO, "Found Manager: " + cf + "  with employers : " + counter);
            }

        } catch (SQLException e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return ls;
    }

}

package it.techgap.challenge.java.senior;

public class Challenge06Extra {
    public static String splitAndReverse(String i, int positions) {
        if (i == null) return null;
        String reverseWord = "";
        String remain = "";
        for (int j = 0; j < positions; j++) {
            remain = remain + i.charAt(j);
        }
        for (int j = i.length() - 1; j >= positions; j--) {
            reverseWord = reverseWord + i.charAt(j);
        }
        return new StringBuilder().append(reverseWord).append(remain).toString();
    }
}

package it.techgap.challenge.java.senior.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ipassari on 15/01/2018.
 */
public class ValueCounterImpl<V> implements ValueCounter<V> {

    List<V> list = new ArrayList<V>();

    @Override
    public void addValue(V value) {
        list.add(value);
    }

    @Override
    public int howMany(V value) {
        return (int) list.stream().filter(v -> v.equals(value)).count();
    }
}

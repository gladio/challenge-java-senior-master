package it.techgap.challenge.java.senior.beans;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by ipassari on 15/01/2018.
 */
public class TimeBasedKVCacheImpl<K, V> implements TimeBasedKVCache<K, V> {

    private int maxElements = 0;
    private long duration;
    private long startTimeCache;
    private TimeUnit timeunit;
    final LinkedHashMap<K, V> cache = new LinkedHashMap<K, V>() {
        @Override
        protected boolean removeEldestEntry(final Map.Entry eldest) {
            return size() > maxElements;
        }
    };

    @Override
    public void setMaximumElements(int elements) {
        this.maxElements = elements;
    }

    @Override
    public void setElementsTimeToLive(long duration, TimeUnit timeunit) {
        this.duration = duration;
        this.timeunit = timeunit;
    }

    @Override
    public V getValue(K key) {
        long diff = timeunit.toMillis(System.currentTimeMillis() - startTimeCache);
        if (diff >= duration) {
            cache.clear();
        } else return cache.get(key);
        return null;
    }

    @Override
    public void addValue(K key, V value) {
        if (cache.size() == 0) startTimeCache = System.currentTimeMillis();
        cache.put(key, value);
    }
}

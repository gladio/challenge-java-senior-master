package it.techgap.challenge.java.senior;


import java.util.*;
import java.util.stream.Collectors;

public class Challenge01CollectionMethods {

    public static int max(int[] in) {
        List<Integer> list = Arrays.stream(in).boxed().collect(Collectors.toList());
        return Collections.max(list);
    }

    public static int min(int[] in) {
        List<Integer> list = Arrays.stream(in).boxed().collect(Collectors.toList());
        return Collections.min(list);
    }

    public static int[] sortIt(int[] in) {
        Arrays.sort(in);
        return in;
    }

    public static Boolean[] sortIt(Boolean[] in) {
        if (in == null) return null;
        Arrays.sort(in, Collections.reverseOrder());
        return in;
    }

    public static int[] removeNegative(int[] in) {
        return Arrays.stream(in).filter(a -> a >= 0).sorted().toArray();
    }

    public static int count(String[] in, String e) {
        if (in == null || e == null) return 0;
        return (int) Arrays.stream(in).filter(p -> p.equals(e)).count();
    }

    public static int maxRepetitions(String[] in) {
        if (in == null) return 0;
        Comparator<Long> longComparator = Long::compareTo;
        Optional<Long> max = Arrays.stream(in).collect(Collectors.groupingBy(p -> p, Collectors.counting())).values().stream().max(longComparator);
        return max.map(Long::intValue).orElse(0);
    }

    public static int[] mergeAndSort(int[]... in) {
        return Arrays.stream(in).flatMapToInt(Arrays::stream).sorted().distinct().toArray();
    }

}
